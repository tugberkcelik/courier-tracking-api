CREATE TABLE location_history
(
    id                  UUID                NOT NULL,
    latitude            VARCHAR(20)         NOT NULL,
    longitude           VARCHAR(20)         NOT NULL,
    event_time          timestamptz         NOT NULL,
    store_id            UUID                NOT NULL,
    courier_id          UUID                NOT NULL,

    version             SMALLINT            NOT NULL,
    created_by          VARCHAR(255)        NOT NULL,
    created_date        timestamptz         NOT NULL,
    last_modified_by    VARCHAR(255)        NOT NULL,
    last_modified_date  TIMESTAMPTZ         NOT NULL,

    FOREIGN KEY (store_id) REFERENCES store (id),
    FOREIGN KEY (courier_id) REFERENCES courier (id),

    PRIMARY KEY (id)
)
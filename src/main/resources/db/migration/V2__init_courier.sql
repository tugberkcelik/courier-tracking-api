CREATE TABLE courier
(
    id   UUID         NOT NULL,
    name VARCHAR(255) NOT NULL,

    version SMALLINT NOT NULL,
    created_by VARCHAR(255),
    created_date timestamptz NOT NULL,
    last_modified_by VARCHAR(255) NOT NULL,
    last_modified_date TIMESTAMPTZ NOT NULL,

    PRIMARY KEY (id)
)
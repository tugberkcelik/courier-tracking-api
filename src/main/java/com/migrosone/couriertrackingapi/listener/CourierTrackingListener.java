package com.migrosone.couriertrackingapi.listener;

import com.migrosone.couriertrackingapi.exception.CourierNotFoundException;
import com.migrosone.couriertrackingapi.exception.ErrorMessage;
import com.migrosone.couriertrackingapi.mapper.CourierMapper;
import com.migrosone.couriertrackingapi.model.CourierTrackingEvent;
import com.migrosone.couriertrackingapi.persistence.entity.Courier;
import com.migrosone.couriertrackingapi.persistence.entity.LocationHistory;
import com.migrosone.couriertrackingapi.persistence.entity.Store;
import com.migrosone.couriertrackingapi.persistence.repository.CourierRepository;
import com.migrosone.couriertrackingapi.persistence.repository.LocationHistoryRepository;
import com.migrosone.couriertrackingapi.persistence.repository.StoreRepository;
import com.migrosone.couriertrackingapi.util.Haversine;
import com.migrosone.couriertrackingapi.util.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import java.time.Instant;
import java.util.List;

@Component
public class CourierTrackingListener {
    private final StoreRepository storeRepository;
    private final CourierRepository courierRepository;
    private final LocationHistoryRepository locationHistoryRepository;
    private final CourierMapper mapper;

    @Autowired
    public CourierTrackingListener(StoreRepository storeRepository, CourierRepository courierRepository, LocationHistoryRepository locationHistoryRepository, CourierMapper mapper) {
        this.storeRepository = storeRepository;
        this.courierRepository = courierRepository;
        this.locationHistoryRepository = locationHistoryRepository;
        this.mapper = mapper;
    }

    @KafkaListener(topics = "courier.location.info", groupId = "test2")
    public void consume(CourierTrackingEvent event) {
        Logger.info("courier event consumed", this.getClass());

        try {
            Instant now = Instant.now();
            Instant nowMinus = now.minusSeconds(60);
            List<LocationHistory> historiesFromLastMinute = locationHistoryRepository.findAllByEventTimeBetween(nowMinus, now);

            Courier courier = courierRepository.findById(event.getId()).orElseThrow(()-> new CourierNotFoundException(ErrorMessage.COURIER_NOT_FOUND));

            List<Store> stores = storeRepository.findAll();

            stores.forEach(store -> {
                double storeLat = Double.valueOf(store.getLatitude());
                double storeLong = Double.valueOf(store.getLongitude());

                double courierLat = Double.valueOf(event.getLatitude());
                double courierLong = Double.valueOf(event.getLongitude());

                double distanceInMeter = Haversine.distance(storeLat, courierLat, storeLong, courierLong, 0, 0);

                if (distanceInMeter <= 100) {
                    LocationHistory history = new LocationHistory();

                    if (historiesFromLastMinute.stream().noneMatch(l -> l.getStore().getId().equals(store.getId()))) {
                        history.setStore(store);
                        history.setCourier(courier);
                        history.setLatitude(event.getLatitude());
                        history.setLongitude(event.getLongitude());
                        history.setEventTime(now);
                        locationHistoryRepository.save(history);
                        Logger.info("courier location history added", this.getClass());
                    }
                }

            });

            Logger.info("all stores traversal and added courier to < 100 meters", this.getClass());
        }
        catch (Exception exception) {
            Logger.error("exception occured while listening courier track event reason: "+ exception.getMessage(), this.getClass());
        }
    }
}

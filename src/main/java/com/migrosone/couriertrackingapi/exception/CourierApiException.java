package com.migrosone.couriertrackingapi.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class CourierApiException extends RuntimeException {

    private Integer code;
    private HttpStatus status;
    private Object[] args;
    private LocalDateTime timestamp = null;
    private String exception;
    private String path;

    public CourierApiException(HttpStatus status, ErrorMessage message) {
        super(message.getValue());
        this.status = status;
        this.code = status.value();
    }
}

package com.migrosone.couriertrackingapi.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@Builder
public class ErrorResponse {
    private Integer code;
    private String message;
    private LocalDateTime timestamp;
    private String exception;
    private String path;
}

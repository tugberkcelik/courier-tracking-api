package com.migrosone.couriertrackingapi.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class CourierNotFoundException extends CourierApiException{

    private ErrorMessage errorMessage;

    public CourierNotFoundException(ErrorMessage message) {
        super(HttpStatus.NOT_FOUND, message);
        this.errorMessage= message;
    }

}

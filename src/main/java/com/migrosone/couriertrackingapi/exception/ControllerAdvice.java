package com.migrosone.couriertrackingapi.exception;

import com.migrosone.couriertrackingapi.util.Logger;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class ControllerAdvice {

    private final MessageSource messageSource;

    @Autowired
    public ControllerAdvice(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> exception(Exception exception, HttpServletRequest request) {
        ErrorResponse errorResponse = ErrorResponse.builder()
                .code(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .path(request.getRequestURI()).timestamp(LocalDateTime.now())
                .message("generic exception occurred for details: " + exception.getMessage())
                .build();

        Logger.error("generic error occured for details: " + exception.getMessage(), this.getClass());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> exception(MethodArgumentNotValidException exception, HttpServletRequest request) {

        ErrorResponse errorResponse = ErrorResponse.builder()
                .code(exception.getStatusCode()
                .value())
                .exception(exception.getClass().getSimpleName())
                .path(request.getRequestURI()).timestamp(LocalDateTime.now())
                .message(exception.getMessage())
                .build();

        return ResponseEntity.status(exception.getStatusCode()).body(errorResponse);
    }

    @ExceptionHandler(CourierNotFoundException.class)
    public ResponseEntity<ErrorResponse> exception(CourierNotFoundException exception, HttpServletRequest request) {

        String exceptionMessage = messageSource.getMessage(exception.getMessage(), exception.getArgs(), LocaleContextHolder.getLocale());

        ErrorResponse errorResponse = ErrorResponse.builder()
                .code(exception.getStatus().value())
                .exception(exception.getClass().getSimpleName())
                .path(request.getRequestURI())
                .timestamp(LocalDateTime.now())
                .message(exceptionMessage)
                .build();

        Logger.error("exception occured detail: courier not found" , this.getClass());
        return ResponseEntity.status(exception.getStatus()).body(errorResponse);
    }

}

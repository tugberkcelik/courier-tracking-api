package com.migrosone.couriertrackingapi.exception;

public enum ErrorMessage {
    INVALID("error.invalid"),
    COURIER_NOT_FOUND("error.courier.not-found");

    private final String value;

    ErrorMessage(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "ErrorMessage{" +
                "name='" + value + '\'' +
                '}';
    }
}

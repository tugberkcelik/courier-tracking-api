package com.migrosone.couriertrackingapi.controller;

import com.migrosone.couriertrackingapi.model.CourierResponse;
import com.migrosone.couriertrackingapi.model.CreateCourierRequest;
import com.migrosone.couriertrackingapi.model.TotalDistanceResponse;
import com.migrosone.couriertrackingapi.service.CourierService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(CourierController.RESOURCE)
public class CourierController {
    public static final String RESOURCE = "couriers";

    private final CourierService service;

    @Autowired
    public CourierController(CourierService service) {
        this.service = service;
    }

    @PostMapping
    public CourierResponse createCourier(@RequestBody @Valid CreateCourierRequest request) {
        return service.create(request);
    }

    @GetMapping("/{id}")
    public TotalDistanceResponse totalDistance(@PathVariable UUID id) {
        return service.getTotalTravelDistance(id);
    }
}

package com.migrosone.couriertrackingapi.controller;

import com.migrosone.couriertrackingapi.model.*;
import com.migrosone.couriertrackingapi.persistence.entity.Store;
import com.migrosone.couriertrackingapi.service.CourierService;
import com.migrosone.couriertrackingapi.service.StoreService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping(StoreController.RESOURCE)
public class StoreController {
    public static final String RESOURCE = "stores";

    private final StoreService service;

    @Autowired
    public StoreController(StoreService service) {
        this.service = service;
    }

    @PostMapping
    public StoreResponse createStore(@RequestBody @Valid CreateStoreRequest request) {
        return service.create(request);
    }

}

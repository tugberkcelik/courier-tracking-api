package com.migrosone.couriertrackingapi.controller;

import com.migrosone.couriertrackingapi.model.CourierTrackingEvent;
import com.migrosone.couriertrackingapi.model.CourierEventRequest;
import com.migrosone.couriertrackingapi.service.CourierNotificationService;
import com.migrosone.couriertrackingapi.util.Logger;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(CourierEventSenderController.RESOURCE)
public class CourierEventSenderController {

    public static final String RESOURCE = "track";

    private final CourierNotificationService notificationService;

    @Autowired
    public CourierEventSenderController(CourierNotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @PostMapping
    public String sendEvent(@RequestBody @Valid CourierEventRequest request){
        Logger.info("sample courier location event sending", this.getClass());

        CourierTrackingEvent event = new CourierTrackingEvent(request.getId(), request.getName(), request.getLatitude(), request.getLongitude(), request.getEventTime());
        notificationService.notifyEvent(event);

        Logger.info("sample courier location event sent", this.getClass());

        return "event sent to kafka";
    }

}

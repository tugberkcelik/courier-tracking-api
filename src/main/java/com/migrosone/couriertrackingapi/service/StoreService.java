package com.migrosone.couriertrackingapi.service;

import com.migrosone.couriertrackingapi.exception.CourierNotFoundException;
import com.migrosone.couriertrackingapi.exception.ErrorMessage;
import com.migrosone.couriertrackingapi.mapper.StoreMapper;
import com.migrosone.couriertrackingapi.model.*;
import com.migrosone.couriertrackingapi.persistence.entity.Courier;
import com.migrosone.couriertrackingapi.persistence.entity.LocationHistory;
import com.migrosone.couriertrackingapi.persistence.entity.Store;
import com.migrosone.couriertrackingapi.persistence.repository.StoreRepository;
import com.migrosone.couriertrackingapi.util.Haversine;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class StoreService {
    private final StoreRepository repository;
    private final StoreMapper mapper;

    @Autowired
    public StoreService(StoreRepository repository, StoreMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public StoreResponse create(@Valid CreateStoreRequest request) {
        Store store = mapper.toStore(request);
        Store savedStore = repository.save(store);
        return mapper.toStoreResponse(savedStore);
    }
}



package com.migrosone.couriertrackingapi.service;

import com.migrosone.couriertrackingapi.model.CourierTrackingEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class CourierNotificationService {

    final KafkaTemplate<String, Object> kafkaTemplate;

    @Autowired
    public CourierNotificationService(KafkaTemplate<String, Object> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void notifyEvent(CourierTrackingEvent event) {
        kafkaTemplate.send("courier.location.info", event);
    }
}

package com.migrosone.couriertrackingapi.service;

import com.migrosone.couriertrackingapi.exception.CourierNotFoundException;
import com.migrosone.couriertrackingapi.exception.ErrorMessage;
import com.migrosone.couriertrackingapi.model.Point;
import com.migrosone.couriertrackingapi.persistence.entity.Courier;
import com.migrosone.couriertrackingapi.persistence.entity.LocationHistory;
import com.migrosone.couriertrackingapi.mapper.CourierMapper;
import com.migrosone.couriertrackingapi.model.CourierResponse;
import com.migrosone.couriertrackingapi.model.CreateCourierRequest;
import com.migrosone.couriertrackingapi.model.TotalDistanceResponse;
import com.migrosone.couriertrackingapi.persistence.repository.CourierRepository;
import com.migrosone.couriertrackingapi.util.Haversine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service
public class CourierService {

    private final CourierRepository repository;

    private final CourierMapper mapper;

    @Autowired
    public CourierService(CourierRepository repository, CourierMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public CourierResponse create(CreateCourierRequest request) {
        Courier courier = mapper.toCourier(request);
        Courier savedCourier = repository.save(courier);
        return mapper.toCourierResponse(savedCourier);
    }

    public TotalDistanceResponse getTotalTravelDistance(UUID id) {
        Courier courier = repository.findById(id).orElseThrow(() -> new CourierNotFoundException(ErrorMessage.COURIER_NOT_FOUND));

        Set<LocationHistory> locationHistories = courier.getLocationHistories();

        List<Point> points = locationHistories.stream().map(history -> new Point(Double.valueOf(history.getLatitude()), Double.valueOf(history.getLongitude()))).toList();

        double totalDistance = calculateDistance(points);

        return mapper.toTotalDistanceResponse(courier, totalDistance);
    }

    private double calculateDistance(List<Point> points) {
        if (points.isEmpty() || points.size() == 1) {
            return 0.00;
        }

        Point firstLocation = points.get(0);

        double totalDistance = 0;

        for (int i = 1; i < points.size(); i++) {
            Point location = points.get(i);
            if (!location.equals(firstLocation)) {
                double distanceOfTwoPoint = Haversine.distance(firstLocation.getLatitude(), location.getLatitude(), firstLocation.getLongitude(), location.getLongitude(), 0,0);
                totalDistance += distanceOfTwoPoint;
            }
        }

        return totalDistance;
    }

}



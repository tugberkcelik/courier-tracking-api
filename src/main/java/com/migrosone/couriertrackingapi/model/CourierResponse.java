package com.migrosone.couriertrackingapi.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class CourierResponse {
    private UUID id;
    private String name;
}

package com.migrosone.couriertrackingapi.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
public class CourierEventRequest {
    private UUID id;

    @NotBlank
    private String name;

    @Pattern(regexp = "^(-?[1-8]?[0-9](\\.\\d+)?|90(\\.0+)?)$", message = "error.courier.latitude.invalid")
    private String latitude;

    @Pattern(regexp = "^(-?(1[0-7][0-9]|[1-9]?[0-9])(\\.\\d+)?|180(\\.0+)?)$", message = "error.courier.longitude.invalid")
    private String longitude;

    private Instant eventTime;
}

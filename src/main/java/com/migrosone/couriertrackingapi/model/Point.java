package com.migrosone.couriertrackingapi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Point {
    private Double latitude;
    private Double longitude;
}

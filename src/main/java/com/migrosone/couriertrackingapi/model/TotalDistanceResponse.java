package com.migrosone.couriertrackingapi.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class TotalDistanceResponse {
    private UUID courierId;
    private String name;
    private Double totalDistance;
}

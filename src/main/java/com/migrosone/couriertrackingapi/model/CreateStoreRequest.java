package com.migrosone.couriertrackingapi.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateStoreRequest {
    @NotBlank
    private String name;

    @Pattern(regexp = "^(-?[1-8]?[0-9](\\.\\d+)?|90(\\.0+)?)$", message = "error.store.latitude.invalid")
    private String latitude;

    @Pattern(regexp = "^(-?(1[0-7][0-9]|[1-9]?[0-9])(\\.\\d+)?|180(\\.0+)?)$", message = "error.store.longitude.invalid")
    private String longitude;
}

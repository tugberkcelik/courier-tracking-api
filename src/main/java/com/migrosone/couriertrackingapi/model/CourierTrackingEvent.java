package com.migrosone.couriertrackingapi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.time.Instant;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CourierTrackingEvent {
    private UUID id;
    private String name;
    private String latitude;
    private String longitude;
    private Instant eventTime;
}
package com.migrosone.couriertrackingapi.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class StoreResponse {
    private UUID id;
    private String name;
    private String latitude;
    private String longitude;
}

package com.migrosone.couriertrackingapi.mapper;

import com.migrosone.couriertrackingapi.persistence.entity.Courier;
import com.migrosone.couriertrackingapi.model.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CourierMapper {
    Courier toCourier(CreateCourierRequest request);

    Courier toCourier(CourierTrackingEvent event);

    CourierResponse toCourierResponse(Courier courier);

    @Mapping(source = "courier.id", target = "courierId")
    TotalDistanceResponse toTotalDistanceResponse(Courier courier, double totalDistance);
}


package com.migrosone.couriertrackingapi.mapper;

import com.migrosone.couriertrackingapi.model.*;
import com.migrosone.couriertrackingapi.persistence.entity.Courier;
import com.migrosone.couriertrackingapi.persistence.entity.Store;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface StoreMapper {
    Store toStore(CreateStoreRequest request);

    StoreResponse toStoreResponse(Store store);
}


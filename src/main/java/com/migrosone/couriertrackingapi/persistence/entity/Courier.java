
package com.migrosone.couriertrackingapi.persistence.entity;

import jakarta.persistence.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.Instant;
import java.util.*;

@Entity
@Table(name = "courier")
@EntityListeners(value = AuditingEntityListener.class)
public class Courier {

    @Id
    @GeneratedValue
    private UUID id = null;

    @Column(name = "name")
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "courier")
    private Set<LocationHistory> locationHistories = new LinkedHashSet<>();

    @Version
    private Integer version = null;

    @CreatedBy
    private String createdBy = "test";

    @CreatedDate
    private Instant createdDate = null;

    @LastModifiedBy
    private String lastModifiedBy = "test";

    @LastModifiedDate
    private Instant lastModifiedDate;

    public Courier() {}

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Set<LocationHistory> getLocationHistories() {
        return locationHistories;
    }

    public void setLocationHistories(Set<LocationHistory> locationHistories) {
        this.locationHistories = locationHistories;
    }
}


package com.migrosone.couriertrackingapi.persistence.repository;

import com.migrosone.couriertrackingapi.persistence.entity.LocationHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Repository
public interface LocationHistoryRepository extends JpaRepository<LocationHistory, UUID> {
    List<LocationHistory> findAllByEventTimeBetween(Instant from, Instant to);
}

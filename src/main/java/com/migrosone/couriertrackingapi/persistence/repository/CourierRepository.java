
package com.migrosone.couriertrackingapi.persistence.repository;

import com.migrosone.couriertrackingapi.persistence.entity.Courier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CourierRepository extends JpaRepository<Courier, UUID> {
    Optional<Courier> findByName(String name);
}

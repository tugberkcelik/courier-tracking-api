package com.migrosone.couriertrackingapi.util;

import java.util.Random;

public class LatLonGenarator {

    private static final Random random = new Random();

    public static double generateLatitude() {
        // Latitude ranges from -90 to 90
        return -90 + (90 - (-90)) * random.nextDouble();
    }

    public static double generateLongitude() {
        // Longitude ranges from -180 to 180
        return -180 + (180 - (-180)) * random.nextDouble();
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            double latitude = generateLatitude();
            double longitude = generateLongitude();
            System.out.println("Latitude: " + latitude + ", Longitude: " + longitude);
        }
    }
}

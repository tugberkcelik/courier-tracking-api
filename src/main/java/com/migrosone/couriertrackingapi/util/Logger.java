package com.migrosone.couriertrackingapi.util;

import org.slf4j.LoggerFactory;

public class Logger {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(Logger.class);

    public static void info(String msg, Class<?> clazz) {
        logger.info("class: {}, message: {}", clazz.getSimpleName(), msg);
    }

    public static void error(String msg, Class<?> clazz) {
        logger.error("class: {}, message: {}", clazz.getSimpleName(), msg);
    }

    public static void warn(String msg, Class<?> clazz) {
        logger.warn("class: {}, message: {}", clazz.getSimpleName(), msg);
    }

    public static void debug(String msg, Class<?> clazz) {
        logger.debug("class: {}, message: {}", clazz.getSimpleName(), msg);
    }
}

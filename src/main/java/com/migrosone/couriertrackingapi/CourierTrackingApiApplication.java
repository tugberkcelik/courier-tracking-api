package com.migrosone.couriertrackingapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.time.Instant;

@SpringBootApplication
@EnableJpaAuditing
public class CourierTrackingApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CourierTrackingApiApplication.class, args);
    }
}

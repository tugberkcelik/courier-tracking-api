# run dependent services
docker-compose -f ./docker/docker-compose.yml up -d

# build
./mvnw clean install

# jvm process init
java -jar ./target/courier-tracking-api-0.0.1-SNAPSHOT.jar
